/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model.util;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.model.Person;
import org.bibsonomy.util.StringUtils;

/**
 * util methods for {@link Person}
 *
 * @author dzo
 */
public final class PersonUtils {
	private PersonUtils() {}
	
	/**
	 * generates the base of person identifier
	 * @param person
	 * @return the base of the person identifier
	 */
	public static String generatePersonIdBase(final Person person) {
		final String firstName = person.getMainName().getFirstName();
		final String lastName  = person.getMainName().getLastName();
		
		if (!present(lastName)) {
			throw new IllegalArgumentException("lastName may not be empty");
		}
		
		final StringBuilder sb = new StringBuilder();
		if (present(firstName)) {
			sb.append(normName(firstName).charAt(0));
			sb.append('.');
		}
		sb.append(normName(lastName));
	
		return sb.toString();
	}

	/**
	 * @param name
	 * @return
	 */
	private static String normName(final String name) {
		return StringUtils.foldToASCII(name.trim().toLowerCase().replaceAll("\\s", "_"));
	}
}
