/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.testutil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Tag;
import org.junit.Test;

/**
 * @author Jens Illig
 */
public class ModelUtilsTest {

	/**
	 * tests assertPropertyEquality
	 */
	@Test
	public void assertPropertyEquality() {
		final Post<BibTex> postA = ModelUtils.generatePost(BibTex.class);
		final Post<BibTex> postB = ModelUtils.generatePost(BibTex.class);
		CommonModelUtils.assertPropertyEquality(postA, postB, Integer.MAX_VALUE, null, "date");
		postB.getTags().clear();
		try {
			CommonModelUtils.assertPropertyEquality(postA, postB, Integer.MAX_VALUE, null);
			fail();
		} catch (final Throwable ignored) {
		}
		try {
			CommonModelUtils.assertPropertyEquality(postA, postB, Integer.MAX_VALUE, Pattern.compile(".ate"));
			fail();
		} catch (final Throwable ignored) {
		}
		postB.setDate(postA.getDate());
		CommonModelUtils.assertPropertyEquality(postA, postB, 1, null);
		CommonModelUtils.assertPropertyEquality(postA, postB, Integer.MAX_VALUE, null, "tags");
		CommonModelUtils.assertPropertyEquality(postA, postB, Integer.MAX_VALUE, Pattern.compile("t[ga]{2}s"));
	}
	
	/**
	 * tests {@link ModelUtils#getTagSet(String...)}
	 */
	@Test
	public void getTagSet() {
		final Set<Tag> expected = new HashSet<Tag>();
		expected.add(new Tag("test"));
		expected.add(new Tag("test2"));
		expected.add(new Tag("test3"));
		
		assertEquals(expected, ModelUtils.getTagSet("test", "test2", "test3"));
		
	}
}