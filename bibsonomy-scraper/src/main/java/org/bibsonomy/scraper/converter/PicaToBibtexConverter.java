/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.converter;


import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.scraper.converter.picatobibtex.PicaParser;
import org.bibsonomy.scraper.converter.picatobibtex.PicaRecord;
import org.bibsonomy.scraper.converter.picatobibtex.Row;
import org.bibsonomy.scraper.exceptions.ScrapingException;

/**
 * @author C. Kramer
 */
public class PicaToBibtexConverter implements BibtexConverter {
	private static final Log log = LogFactory.getLog(PicaToBibtexConverter.class);
	
	private final static Pattern PATTERN_LONGTITLE = Pattern.compile("(?s)<LONGTITLE.*?>(.*)</LONGTITLE>");
	private final static Pattern PATTERN_PICA_CATEGORY = Pattern.compile("^(\\d{3}[A-Z@]{1}/\\d{2}|\\d{3}[A-Z@]{1})(.+)$");
	private final static Pattern PATTERN_PICA_CATEGORY_SUBFIELD = Pattern.compile("(\\$[0-9a-zA-Z]{1})([^\\$]+)");

	private final PicaRecord pica;
	private final String url;
	
	@Override
	public String toBibtex(final String sc) {
		parseContent(sc);
		return PicaParser.getBibRes(this.pica, this.url);
	}
	/**
	 * Convert the pica content to the pica object structure
	 * 
	 * @param sc 
	 * @param type
	 * @param url
	 */
	public PicaToBibtexConverter(final String type, final String url){
		this.pica = new PicaRecord();
		this.url = url;
	}
	
	/**
	 * Creates an PicaRecord object out of the given content
	 * 
	 * @param sc
	 */
	private void parseContent(final String sc){
		try {
			
			// get the content of the XML tag <longtitle></longtitle>
			final Matcher matcher = PATTERN_LONGTITLE.matcher(sc);
			
			// if there is content save it, if not throw exception
			if (!matcher.find()){
				throw new ScrapingException("Could not extract content");
			}
			
			final String formattedCont = (matcher.group(1));
			
			// divide content by newlines
			final StringTokenizer token = new StringTokenizer(formattedCont, "\n");
			
			// finally create the PICA objects
			while (token.hasMoreTokens()) {
				final String row = token.nextToken();
				if (!"<br />".equals(row)){
					processRow(row);
				}
			}
		} catch (final Exception e) {
			log.error(e);
		}
	}
	
	/**
	 * This method should extract the necessary content with regex and put the information
	 * to the PicaRecord object
	 * 
	 * @param content
	 */
	private void processRow(final String content) {
		// pattern to extract the pica category
		final Matcher matcher = PATTERN_PICA_CATEGORY.matcher(content);
		
		if (matcher.matches()) {
			final String category = matcher.group(1);
			final Row row = new Row(category);
			final String _cont = matcher.group(2);
			
			// extract the subfield of each category
			final Matcher matcherSub = PATTERN_PICA_CATEGORY_SUBFIELD.matcher(_cont);
			
			// put it to the row object
			while (matcherSub.find()) {
				final String subCategory = matcherSub.group(1);
				final String subContent = matcherSub.group(2);
				row.addSubField(subCategory, subContent);
			}

			// and finally put the row to the picarecord
			this.pica.addRow(row);
		}
	}
	
	/**
	 * @return PicaRecord
	 */
	public PicaRecord getActualPicaRecord(){
		return this.pica;
	}
}
