/**
 * BibSonomy Pingback - Pingback/Trackback for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.pingback;

import static org.bibsonomy.util.ValidationUtils.present;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bibsonomy.util.StringUtils;
import org.junit.Ignore;

/**
 * @author rja
 */
@Ignore
public class TestServlet extends HttpServlet {
	private static final String TRACKBACK_ARTICLE_PATH = "/article";
	private static final String CONTENT_TYPE_HTML = "text/html";
	private static final String CHAR_ENCODING = StringUtils.CHARSET_UTF_8;
	public static final String URLHERE = "URLHERE";

	private static final String PINGBACK_HEADER = "X-Pingback";
	private static final String PINGBACK_PATH = "/pingback";
	private static final String PINGBACK_XMLRPC = "/xmlrpc";
	private static final String PINGBACK_HTML = "<link rel=\"pingback\" href=\"" + URLHERE + "\" />\n";

	private static final String TRACKBACK_PATH = "/trackback";


	private static final int[] ERRORS = new int[]{
		HttpServletResponse.SC_FORBIDDEN,
		HttpServletResponse.SC_BAD_GATEWAY,
		HttpServletResponse.SC_BAD_REQUEST,
		HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
		HttpServletResponse.SC_METHOD_NOT_ALLOWED,
		HttpServletResponse.SC_MOVED_PERMANENTLY,
		HttpServletResponse.SC_NO_CONTENT
	};

	public static final String TOP_OF_HTML_PAGE = "" +
	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
	"<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"en-US\">\n" +
	"\n" +
	"    <head profile=\"http://gmpg.org/xfn/11\">\n" +
	"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
	"\n" +
	"    <title>Something blah blah &laquo;  Ping tester</title>\n" +
	"\n";

	public static final String TRACKBACK_RDF1 = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
	"xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" +
	"xmlns:trackback=\"http://madskills.com/public/xml/rss/module/trackback/\">\n" + 
	"<rdf:Description\n" +
	"rdf:about=\"http://www.foo.com/archive.html#foo\"\n" +
	"dc:identifier=\"http://www.foo.com/archive.html#foo\"\n" +
	"dc:title=\"Foo Bar\"\n" +
	"trackback:ping=\"" + URLHERE + "\" />\n" +
	"</rdf:RDF>\n";
	public static final String TRACKBACK_RDF2 = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
	"xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" +
	"xmlns:trackback=\"http://madskills.com/public/xml/rss/module/trackback/\">\n" + 
	"<rdf:Description\n" +
	"rdf:about=\"http://www.foo.com/book.html\"\n" +
	"dc:identifier=\"http://www.foo.com/book.html\"\n" +
	"dc:title=\"Foo Bar\"\n" +
	"trackback:ping=\"" + URLHERE + "\" />\n" +
	"</rdf:RDF>\n";


	private static final long serialVersionUID = 8692283813700271210L;

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		final String pathInfo = request.getPathInfo();

		if (pathInfo.startsWith(PINGBACK_PATH)) {
			final StringBuffer requestURL = request.getRequestURL();

			System.out.println("GET " + requestURL);

			final boolean body   = present(request.getParameter("body"));
			final boolean header = present(request.getParameter("header"));		

			response.setCharacterEncoding(CHAR_ENCODING);
			response.setContentType(CONTENT_TYPE_HTML);

			final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(), CHAR_ENCODING));
			out.write(TOP_OF_HTML_PAGE);

			if (header) {
				response.setHeader(PINGBACK_HEADER, requestURL + PINGBACK_XMLRPC);
			}
			if (body) {
				out.write(PINGBACK_HTML.replace(URLHERE, requestURL + PINGBACK_XMLRPC));
			}
			out.write("\n");
			out.flush();
		} else if (pathInfo.startsWith(TRACKBACK_ARTICLE_PATH)) {
			final StringBuffer requestURL = request.getRequestURL();

			System.out.println("GET " + requestURL);

			response.setCharacterEncoding(CHAR_ENCODING);
			response.setContentType(CONTENT_TYPE_HTML);

			final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(), CHAR_ENCODING));
			out.write(TOP_OF_HTML_PAGE);

			final String rdf = TRACKBACK_RDF1
			.replaceAll("http://www.foo.com/archive.html#foo", requestURL.toString())
			.replaceAll(URLHERE, requestURL + TRACKBACK_PATH);

			out.write(rdf);
			out.write(TRACKBACK_RDF2.replaceAll(URLHERE, requestURL + TRACKBACK_PATH));

			out.write("\n");
			out.flush();
		} else if (pathInfo.startsWith("/stream")) {
			response.setContentType("application/stream");
		} else {
			response.sendError(ERRORS[(int) Math.round(Math.random() * ERRORS.length) - 1]);
		}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException ,IOException {
		final String requestUrl = request.getRequestURL().toString();
		System.out.println("POST " + requestUrl);
		if (requestUrl.endsWith("/xmlrpc")) {
			/*
			 * IN
			 */
			final StringBuilder buf = new StringBuilder();
			final BufferedReader reader = request.getReader();
			String line;
			while ((line = reader.readLine()) != null) {
				buf.append(line + "\n");
			}
			reader.close();
			System.out.println(buf);

			/*
			 * out
			 */
			response.setCharacterEncoding(CHAR_ENCODING);
			response.setContentType("application/xml");
			final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(), CHAR_ENCODING));
			out.write(
					"<?xml version=\"1.0\"?>\n" + 
					"<methodResponse>\n" +
					"<params>\n" +
					"<param>\n" +
					"<value><string>success</string></value>\n" +
					"</param>\n" +
					"</params>\n" +
					"</methodResponse>\n"
			);
			out.flush();
		} else if(requestUrl.endsWith("/trackback")) {

			/*
			 * out
			 */
			response.setCharacterEncoding(CHAR_ENCODING);
			response.setContentType("application/x-www-form-urlencoded");
			final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(), CHAR_ENCODING));

			if (requestUrl.contains(TRACKBACK_ARTICLE_PATH)) {
				out.write(
						"<?xml version=\"1.0\"?>\n" + 
						"<response>\n" +
						"<error>0</error>\n" +
						"</response>\n"
				);
			} else {
				out.write(
						"<?xml version=\"1.0\"?>\n" + 
						"<response>\n" +
						"<error>1</error>\n" +
						"<message>unknown URL</message>\n" +
						"</response>\n"
				);
			}
			out.flush();
		}
	};

}