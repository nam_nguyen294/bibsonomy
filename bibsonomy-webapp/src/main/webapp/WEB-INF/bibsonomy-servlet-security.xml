<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:util="http://www.springframework.org/schema/util"
	xmlns:security="http://www.springframework.org/schema/security"
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
						http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
						http://www.springframework.org/schema/security 
						http://www.springframework.org/schema/security/spring-security-3.2.xsd
						http://www.springframework.org/schema/util 
						http://www.springframework.org/schema/util/spring-util-3.1.xsd">
	
	<security:global-method-security pre-post-annotations="enabled" />
	
	<bean id="loginUrl" class="java.lang.String">
		<constructor-arg value="/login" />
	</bean>
	
	<bean id="authConfig" class="org.bibsonomy.webapp.util.spring.security.AuthMethodListFactoryBean">
		<property name="authConfig" value="${auth.order}" />
	</bean>
	
	<!-- here we config by hand a filter proxy chain (normally done by the namespacehandler of spring security) -->
	<util:list id="org.springframework.security.filterChains" />
	
	<alias name="org.springframework.security.filterChainProxy" alias="springSecurityFilterChain"/>
	
	<bean id="org.springframework.security.filterChainProxy" class="org.bibsonomy.webapp.util.spring.security.FilterChainProxyFactoryBean">
		<property name="config" ref="authConfig" />
		<property name="authFilterMap">
			<map key-type="org.bibsonomy.common.enums.AuthMethod">
				<entry key="INTERNAL">
					<list>
						<ref bean="internalUsernamePasswordFilter" />
					</list>
				</entry>
				<entry key="LDAP">
					<list>
						<ref bean="ldapUsernamePasswordFilter" />
					</list>
				</entry>
				<entry key="SAML">
					<list>
						<ref bean="samlHttpsPretedingFilter"/>
						<ref bean="samlFilter" />
						<ref bean="samlLoginFilter" />
						<ref bean="samlAutoLoginFilter"/>
					</list>
				</entry>
				<entry key="OPENID">
					<list>
						<ref bean="openidFilter" />
					</list>
				</entry>
				<entry key="HTTPBASIC">
					<list>
						<ref bean="httpBasicAuthenticationFilter" />
					</list>
				</entry>
			</map>
		</property>
		<property name="authRememberMeFilterMap">
			<map key-type="org.bibsonomy.common.enums.AuthMethod">
				<entry key="INTERNAL" value-ref="internalRememberMeFilter" />
				<entry key="LDAP" value-ref="ldapRememberMeFilter" />
				<entry key="OPENID" value-ref="openidRememberMeFilter" />
				<entry key="HTTPBASIC" value-ref="internalRememberMeFilter" /><!-- FIXME: does it make sense to use this filter for HTTP Basic Auth? -->
			</map>
		</property>
	</bean>
	
	<!-- no filtering for css and js -->
	<security:http pattern="/resources/**" security="none" />
	<security:http pattern="/resources_puma/**" security="none" />
	<security:http pattern="/logging**" security="none" />
	
	<!-- api uses own authentication handling -->
	<security:http pattern="/api/**" security="none" />
	
	<!-- OAuth temporary tokens may be requested without authentication -->
	<security:http pattern="/oauth/requestToken" security="none" />
	
	<!-- all other sites -->
	<security:http use-expressions="true" entry-point-ref="delegatingEntryPoint" auto-config="false" security-context-repository-ref="contextRepository">		
		<!-- login -->
		<security:intercept-url pattern="/login" access="isAnonymous()" />
		
		<!-- restrict admin pages to admins -->
		<security:intercept-url pattern="/admin/**" access="hasRole('ROLE_ADMIN')" />
		<!-- everything else -->
		<security:intercept-url pattern="/**" access="permitAll" />
		
		<!-- logout filter (deletes cookies, destroys session) -->
		<security:custom-filter position="LOGOUT_FILTER" ref="logoutFilter" />
		
		<!--
			 XXX: it is a little bit tricky to add another exception translation filter after the other one
			 but if we replaced the configurated one we could not use the namespace config (http, intercept-url) and had to add
			 all spring security filters to this config file by bean definitions :(
		 -->
		<security:custom-filter after="EXCEPTION_TRANSLATION_FILTER" ref="exceptionTranslationFilter" />
		<!-- fetches application url for saml metadata generation -->
		<security:custom-filter before="FIRST" ref="samlMetadataGeneratorFilter"/>
		
		<security:custom-filter after="LAST" ref="limitedUserFilter" />

		<!-- 
			login filters are configurated by
			org.bibsonomy.webapp.config.AuthenticationFilterConfigurator
		 -->
	</security:http>
	
	<bean id="limitedUserFilter" class="org.bibsonomy.webapp.filters.LimitedUserFilter">
		<property name="allowedPathPrefixes">
			<list>
				<value>/limitedAccountActivation</value>
				<value>/autoRegisterSamlAndOAuth</value>
				<value>/render</value>
			</list>
		</property>
	</bean>
	
	<!-- enable when using spring security tag libs
	<bean id="expressionHandler" class="org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler" />
	 -->
	 
	<!-- Teergrube for login handler  -->
	<bean id="teergrube" class="org.bibsonomy.webapp.util.TeerGrube" scope="singleton">
		<property name="maxQueueAgeSeconds" value="1800"/>
		<property name="waitingSecondsPerRetry" value="3"/>
	</bean>
	
	<bean id="logoutFilter" class="org.springframework.security.web.authentication.logout.LogoutFilter">
		<constructor-arg type="java.lang.String" value="/" />
		<constructor-arg>
			<list>
				<ref bean="securityContextLogoutHandler" />
				<ref bean="openidRememberMeServices" />
				<ref bean="internalRememberMeServices" />
				<ref bean="ldapRememberMeServices" />
				<ref bean="logoutRedirectHandler" />
			</list>
		</constructor-arg>
		<property name="filterProcessesUrl" value="/logout" />
	</bean>
	
	<bean id="logoutRedirectHandler" class="org.bibsonomy.webapp.util.spring.security.web.logouthandler.RedirectHandler">
		<property name="parameterName" value="redirectKey"/>
		<property name="redirectUrls">
			<bean class="org.bibsonomy.webapp.util.spring.factorybeans.JsonToMapFactoryBean">
				<constructor-arg value="${logout.redirect.map}" />
			</bean>
		</property>
	</bean>
	
	<!-- invalidates the session -->
	<bean id="securityContextLogoutHandler" class="org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler" />
	
	<bean id="contextRepository" class="org.bibsonomy.webapp.util.spring.security.UsernameSecurityContextRepository">
		<property name="service" ref="databaseUserService" />
	</bean>
	
	<bean id="exceptionTranslationFilter" class="org.bibsonomy.webapp.util.spring.security.filter.ExceptionTranslationFilter">
		<property name="authenticationEntryPoint" ref="delegatingEntryPoint" />
		<property name="accessDeniedHandler" ref="accessDeniedHandler" />
	</bean>
	
	<bean id="accessDeniedHandler" class="org.springframework.security.web.access.AccessDeniedHandlerImpl">
		<property name="errorPage" value="/errors/403" />
	</bean>

	<bean id="delegatingEntryPoint" class="org.springframework.security.web.authentication.DelegatingAuthenticationEntryPoint">
		<constructor-arg>
			<map key-type="org.springframework.security.web.util.AntPathRequestMatcher">
				<entry key="/autoRegisterSamlAndOAuth" value-ref="samlEntryPoint"/>
				<entry key="/login_saml" value-ref="samlEntryPoint" />
				<entry key="/registersaml" value-ref="samlEntryPoint" />
				<entry key="/register_saml_success" value-ref="samlEntryPoint" />
			</map>
		</constructor-arg>
		<property name="defaultEntryPoint" ref="entryPoint" />
	</bean>


	<bean id="entryPoint" class="org.bibsonomy.webapp.util.spring.security.authentication.LoginUrlAuthenticationEntryPoint">
		<property name="loginFormUrl" ref="loginUrl" />
	</bean>
	
	<bean id="targetUrlParameter" class="java.lang.String">
		<constructor-arg value="referer" />
	</bean>
	
	<bean id="cookieKey" class="java.lang.String">
		<constructor-arg value="${auth.cookie.cryptkey}" />
	</bean>

	<bean id="abstractUsernamePasswordFilter" class="org.bibsonomy.webapp.util.spring.security.filter.UsernamePasswordAuthenticationFilter" abstract="true">
		<property name="grube" ref="teergrube" />
		<property name="authenticationFailureHandler" ref="failureHandler" />
		<property name="authenticationSuccessHandler" ref="successHandler" />
		<property name="usernameParameter" value="username" />
		<property name="passwordParameter" value="password" />
	</bean>
	
	<bean id="successHandler" class="org.bibsonomy.webapp.util.spring.security.handler.SuccessHandler">
		<property name="targetUrlParameter" ref="targetUrlParameter" />
		<property name="loginFormUrl" ref="loginUrl" />
	</bean>

	
	<bean class="org.bibsonomy.webapp.util.spring.security.exceptionmapper.AdLdapUsernameNotFoundExceptionMapper" name="AdLdapExceptionMapper">
		<property name="redirectUrl" value="/registerLDAP?step=2"/>
	</bean>
	
	<bean class="org.bibsonomy.webapp.util.spring.security.exceptionmapper.LdapUsernameNotFoundExceptionMapper" name="ldapExceptionMapper">
		<property name="redirectUrl" value="/registerLDAP?step=2"/>
	</bean>
	
	<bean id="failureHandler" class="org.bibsonomy.webapp.util.spring.security.handler.FailureHandler" scope="singleton">
		<property name="grube" ref="teergrube" />
		<property name="defaultFailureUrl" ref="loginUrl" />
		<property name="usernameNotFoundExceptionMapper">
			<set>
			
				<ref bean="#{ {'${auth.ldap.authenticationProvider}' eq 'adAuthenticationProvider'} ? 'AdLdapExceptionMapper' : 'ldapExceptionMapper'}" />

				<bean class="org.bibsonomy.webapp.util.spring.security.exceptionmapper.SamlUsernameNotFoundExceptionMapper">
					<property name="redirectUrl" value="/registerSaml?step=2"/>
					<property name="attributeExtractor" ref="samlAttributeExtractor" />
				</bean>
				<bean class="org.bibsonomy.webapp.util.spring.security.exceptionmapper.OpenIdUsernameNotFoundExceptionMapper">
					<property name="redirectUrl" value="/registerOpenID?step=2"/>
				</bean>
			</set>
		</property>
		<property name="redirectStrategy">
			<bean class="org.bibsonomy.webapp.util.spring.security.web.ParameterKeepingRedirectStrategy">
				<property name="parameterNames">
					<list>
						<value>selTab</value>
					</list>
				</property>
			</bean>
		</property>
	</bean>
	
	
	<!-- database login -->
	<bean id="internalUsernamePasswordFilter" parent="abstractUsernamePasswordFilter">
		<property name="authenticationManager" ref="internalAuthenticationManager" />
		<property name="filterProcessesUrl" value="/login_internal" />
		<property name="rememberMeServices" ref="internalRememberMeServices" />
	</bean>
	
	<!-- sets a remember me cookie for the database -->
	<bean id="internalRememberMeServices" class="org.bibsonomy.webapp.util.spring.security.rememberMeServices.TokenBasedRememberMeServices">
		<constructor-arg ref="cookieKey" />
		<constructor-arg ref="databaseUserService" />
		<property name="parameter" ref="rememberMeParameter"/>
		<property name="cookieName" value="db_user" />
		<property name="tokenValiditySeconds" ref="cookieAge" /> <!-- found in main servlet xml -->
	</bean>
	
	<!-- filter for the db remember me cookie -->
	<bean id="internalRememberMeFilter" class="org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter">
		<property name="rememberMeServices" ref="internalRememberMeServices" />
		<property name="authenticationManager" ref="internalAuthenticationManager" />
	</bean>
	
	<bean id="databaseUserService" class="org.bibsonomy.webapp.util.spring.security.userdetailsservice.DatabaseUserDetailsService">
		<property name="adminLogic" ref="adminLogic" />
	</bean>	
	
	<security:authentication-manager alias="internalAuthenticationManager">
		<security:authentication-provider ref="databaseAuthenticator" />
		<security:authentication-provider ref="databaseAuthenticatorWithoutEncoding" />
		<security:authentication-provider ref="rememberMeAuthenticator" />
	</security:authentication-manager>
	
	<bean id="abstractDatabaseAuthenticator" class="org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider" abstract="true">
		<property name="userDetailsService" ref="databaseUserService"/>
	</bean>
	
	<bean id="databaseAuthenticator" class="org.springframework.security.authentication.dao.DaoAuthenticationProvider" parent="abstractDatabaseAuthenticator">
		<property name="passwordEncoder">
			<bean class="org.bibsonomy.webapp.util.spring.security.encoding.Md5PasswordEncoder"/>
		</property>
		<property name="saltSource">
			<bean class="org.springframework.security.authentication.dao.ReflectionSaltSource">
				<property name="userPropertyToUse" value="passwordSalt" />
			</bean>
		</property>
	</bean>
	
	<!--+
		| Required when the user shall be logged in after successful activation
		| to compare the already md5hashed password from the pendingUser with
		| the also md5-hashed password from the user table.   
		|
		| Note that this also bears some problems: if someone knows another user's
		| password hash, he can use it to login!
		+-->
	<bean id="databaseAuthenticatorWithoutEncoding" class="org.springframework.security.authentication.dao.DaoAuthenticationProvider" parent="abstractDatabaseAuthenticator" />
		
	<bean id="rememberMeAuthenticator" class="org.springframework.security.authentication.RememberMeAuthenticationProvider">
		<property name="key" ref="cookieKey" />
	</bean>
	
	
	<!--+
		|
		| HTTP Basic Authentication
		| 
		| is blocked by the front proxy except for HTTPS requests
		|
		+-->
	<bean id="httpBasicAuthenticationFilter" class="org.springframework.security.web.authentication.www.BasicAuthenticationFilter">
  		<property name="authenticationManager" ref="internalAuthenticationManager"/>
  		<property name="authenticationEntryPoint" ref="authenticationEntryPoint"/>
	</bean>

	<bean id="authenticationEntryPoint" class="org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint">
		<property name="realmName" value="${project.name}"/>
	</bean>
	
	<!-- Open ID login -->
	<bean id="openidLoginUrl" class="java.lang.String">
		<constructor-arg value="/login_openid" />
	</bean>
	
	<!--+
		| open id consumer
		|
		| TODO: The openid-login XML element of Spring security allows to 
		|       configure OpenID attribute exchange. We should try to use it,
		|       because it's much nicer to write/read.
		+-->
	<bean id="openidConsumer" class="org.springframework.security.openid.OpenID4JavaConsumer">
		<constructor-arg>
			<list>
				<!--+
					|
					| Types with "http://axschema.org/..." don't seem to work - 
					| replaced them with http://schema.openid.net/ and it worked
					| on myopenid.net. See also http://forum.springsource.org/showthread.php?t=85078
					|
				 	+-->
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="nickname"/>
					<constructor-arg index="1" value="http://schema.openid.net/namePerson/friendly"/>
					<property name="required" value="true"/>
				</bean>
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="email"/>
					<constructor-arg index="1" value="http://schema.openid.net/contact/email"/>
					<property name="required" value="true"/>
				</bean>
				
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="fullname"/>
					<constructor-arg index="1" value="http://schema.openid.net/namePerson"/>
				</bean>
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="gender"/>
					<constructor-arg index="1" value="http://schema.openid.net/person/gender"/>
				</bean>
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="language"/>
					<constructor-arg index="1" value="http://schema.openid.net/pref/languagehome"/>
				</bean>
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="city"/>
					<constructor-arg index="1" value="http://schema.openid.net/contact/city/home"/>
				</bean>
				<bean class="org.springframework.security.openid.OpenIDAttribute">
					<constructor-arg index="0" value="web"/>
					<constructor-arg index="1" value="http://schema.openid.net/contact/web/default"/>
				</bean>
			</list>
		</constructor-arg>
	</bean>
	
	<!-- filter -->
	<bean id="openidFilter" class="org.springframework.security.openid.OpenIDAuthenticationFilter">
		<property name="authenticationManager" ref="openidAuthenticationManager" />
		<property name="filterProcessesUrl" ref="openidLoginUrl" />
		<property name="authenticationSuccessHandler" ref="successHandler" />
		<property name="authenticationFailureHandler" ref="failureHandler" />
		<property name="rememberMeServices" ref="openidRememberMeServices" />
		<property name="consumer" ref="openidConsumer" />
		<property name="claimedIdentityFieldName" value="openID" />
		<property name="returnToUrlParameters" ref="openIdReturnParameters" />
	</bean>
	
	<util:set id="openIdReturnParameters">
		<ref bean="targetUrlParameter" />
		<ref bean="rememberMeParameter" />
	</util:set>
	
	<bean id="rememberMeParameter" class="java.lang.String">
		<constructor-arg value="rememberMe" />
	</bean>
	
	<!-- open id remember me filter -->
	<bean id="openidRememberMeFilter" class="org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter">
		<property name="rememberMeServices" ref="openidRememberMeServices" />
		<property name="authenticationManager" ref="openidAuthenticationManager" />
	</bean>
	
	<!-- open id remember me service -->
	<bean id="openidRememberMeServices" class="org.bibsonomy.webapp.util.spring.security.rememberMeServices.OpenIDRememberMeServices">
		<constructor-arg ref="cookieKey" />
		<constructor-arg ref="databaseUserService" />
		<property name="parameter" ref="rememberMeParameter" />
		<property name="cookieName" value="openID_user" />
		<property name="filterUrl" ref="openidLoginUrl" />
		<property name="projectRoot" value="${project.home}" />
		<property name="consumer" ref="openidConsumer" />
		<property name="returnToUrlParameters" ref="openIdReturnParameters" />
	</bean>
	
	<bean id="openidAuthenticationProvider" class="org.bibsonomy.webapp.util.spring.security.provider.OpenIDAuthenticationProvider">
		<property name="authenticationUserDetailsService" ref="openidAuthenticationUserDetailsService" />
	</bean>
	
	<bean id="openidAuthenticationUserDetailsService" class="org.bibsonomy.webapp.util.spring.security.userdetails.OpenIdAuthenticationUserDetailsService">
		<property name="adminLogic" ref="adminLogic" />
	</bean>
	
	<bean id="openidAuthenticationManager" class="org.springframework.security.authentication.ProviderManager">
		<property name="providers">
			<list>
				<ref bean="openidAuthenticationProvider"/> 
			</list>
		</property>
	</bean>
	
	<!-- LDAP login -->
	<bean id="ldapUsernamePasswordFilter" parent="abstractUsernamePasswordFilter">
		<property name="authenticationManager" ref="ldapAuthenticationManager" />
		<property name="filterProcessesUrl" value="/login_ldap" />
		<property name="rememberMeServices" ref="ldapRememberMeServices" />
	</bean>
	
	<!-- configure the used authenticator using the property "auth.ldap.authenticator" -->
	<alias name="${auth.ldap.authenticationProvider}" alias="custLdapAuthProvider"/>
	
	<bean id="ldapAuthenticationManager" class="org.springframework.security.authentication.ProviderManager">
		<property name="providers">
			<list>
				<ref bean="custLdapAuthProvider" /> 
			</list>
		</property>
		<!-- we need the password for the rememeber me cookie (the coookie is encrypted) -->
		<property name="eraseCredentialsAfterAuthentication" value="false" />
	</bean>
	
	<!-- filter for the ldap remember me cookie -->
	<bean id="ldapRememberMeFilter" class="org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter">
		<property name="rememberMeServices" ref="ldapRememberMeServices" />
		<property name="authenticationManager" ref="ldapAuthenticationManager" />
	</bean>
	
	<!-- ldap remember me service -->
	<bean id="ldapRememberMeServices" class="org.bibsonomy.webapp.util.spring.security.rememberMeServices.LDAPRememberMeServices">
  		<constructor-arg ref="cookieKey" />
		<constructor-arg ref="databaseUserService" />
  		<property name="parameter" ref="rememberMeParameter" />
  		<property name="cookieName" value="ldap_user" />
  		<property name="encryptor" ref="encryptor"/>
	</bean>
	
	<bean id="encryptor" class="org.jasypt.util.text.StrongTextEncryptor">
		<property name="password" value="${auth.cookie.cryptkey}" />
	</bean>
	
	<!--+ 
		|
		| Authenticator config for LDAP
		| We have two options: directly create the DN pattern to find the user
		| (simpleLdapBindAuthenticator) or first do a search using a filter
		| (searchLdapBindAuthenticator).
		|
		+-->
	<bean id="simpleLdapBindAuthenticator" class="org.springframework.security.ldap.authentication.BindAuthenticator">
		<constructor-arg ref="contextSource" />
		<property name="userDnPatterns">
			<list>
				<value>${auth.ldap.userDnPattern}</value>
			</list>
		</property>
	</bean>
	<bean id="ldapUserSearch" class="org.springframework.security.ldap.search.FilterBasedLdapUserSearch">
		<constructor-arg value="${auth.ldap.userSearchBase}" />
		<constructor-arg value="${auth.ldap.userSearchFilter}" />
		<constructor-arg ref="contextSource" />
	</bean>
	<bean id="searchLdapBindAuthenticator" class="org.springframework.security.ldap.authentication.BindAuthenticator">
		<constructor-arg ref="contextSource" />
		<property name="userSearch" ref="ldapUserSearch" />
	</bean>
	<!-- configure the used authenticator using the property "auth.ldap.authenticator" -->
	<alias name="${auth.ldap.authenticator}" alias="ldapAuthenticator"/>

	<bean id="ldapAuthProvider" class="org.springframework.security.ldap.authentication.LdapAuthenticationProvider">
		<constructor-arg ref="ldapAuthenticator" />
		<property name="userDetailsContextMapper" ref="ldapUserDetailsMapper" />
		<property name="hideUserNotFoundExceptions" value="false" />
	</bean>
	
	<!-- for testing @see dev wiki
	<security:ldap-server port="33389" ldif="classpath:ldap_test.ldif" root="dc=ub,dc=uni-kassel,dc=de" id="contextSource" />-->
	
	<security:ldap-server id="contextSource" url="${auth.ldap.server}" />
	
	<bean id="ldapUserDetailsMapper" class="org.bibsonomy.webapp.util.spring.security.userdetailsservice.LDAPUserDetailsServiceMapper">
		<property name="userDetailsService" ref="databaseUserService" />
		<property name="adminLogic" ref="adminLogic" />
	</bean>
	
	<!-- AD Login -->
	<!-- configure the provider using the properties "auth.ad.domain" and "auth.ad.url"  -->
	<bean id="adAuthenticationProvider" class="org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider">
		<constructor-arg value="${auth.ad.domain}" />
		<constructor-arg value="${auth.ad.url}" />
		<property name="userDetailsContextMapper" ref="ldapUserDetailsMapper" />
	</bean>
	
</beans>