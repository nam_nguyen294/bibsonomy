<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	>

	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" />
	<jsp:directive.attribute name="userLoggedIn" type="java.lang.Boolean" required="true" />
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="buttonClasses" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="enforceCommunityPost" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="forceCommunityCreate" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="icon" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="titleKey" type="java.lang.String" required="false"/>
	
	<c:if test="${empty icon}">
		<c:set var="icon" value="copy" />
	</c:if>
	
	<c:set var="isCommunityPost" value="${enforceCommunityPost or empty post.user.name}" />
	
	<c:if test="${empty titleKey}">
		<c:choose>
			<c:when test="${isCommunityPost}">
				<c:set var="titleKey" value="post.actions.copy.title" />
			</c:when>
			<c:otherwise>
				<c:set var="titleKey" value="${resourceType}.actions.copy.title" />
			</c:otherwise>
		</c:choose>
		
	</c:if>
	
	<c:set var="isRemotePost" value="${not empty post.systemUrl and not mtl:isSameHost(post.systemUrl, properties['project.home'])}"/>

	<!-- TODO: use the urlGenerator for this -->
	<c:set var="url" value="Publication"/>
	<c:if test="${resourceType eq 'bookmark'}">
		<c:set var="url" value="Bookmark"/>
	</c:if>
	<c:set var="copyTitle" value="" />
	
	<c:choose>
		<c:when test="${isRemotePost and resourceType ne 'bookmark'}">
			<!-- FIXME: remove old url calling -->
			<c:url var="url" value="/BibtexHandler">
				<c:param name="requTask" value="upload"/>
				<c:param name="url" value="${urlGeneratorFactory.createURLGeneratorForSystem(post.systemUrl).getPublicationUrlByIntraHashAndUsername(post.resource.intraHash, post.user.name)}"/>
				<c:param name="description" value="${post.description}"/>
				<c:param name="selection"/>
				<c:param name="referer" value="${mtl:cleanUrl(post.resource.url)}"/>
			</c:url>
		</c:when>
		<c:when test="${isRemotePost and resourceType eq 'bookmark'}">
			<!-- FIXME: remove old url calling -->
			<c:url var="url" value="/ShowBookmarkEntry">
				<c:param name="url" value="${mtl:cleanUrl(post.resource.url)}"/>
				<c:param name="description" value="${post.description}"/>
				<c:param name="extended"/>
				<c:param name="referer" value="${mtl:cleanUrl(post.resource.url)}"/>
			</c:url>
			<a class="${buttonClasses}" href="${url}" title="${copyTitle}">
				<span class="fa fa-copy"><!--  --></span>
				<span class="sr-only"><fmt:message key="post.actions.copy"/></span>
			</a>
		</c:when>
		<c:when test="${systemReadOnly}">
			<c:set var="buttonClasses" value="${buttonClasses} disabled" />
			<fmt:message var="copyTitle" key="system.readOnly.info" />
		</c:when>
		<c:when test="${isCommunityPost}">
			<!-- community-post -->
			<fmt:message key="${titleKey}" var="copyTitle"/>
			<c:url var="url" value="/edit${url}">
				<c:param name="hash" value="${post.resource.interHash}"/>
			</c:url>
		</c:when>
		<c:otherwise>
			<!-- user is not post's owner -->
			<c:choose>
				<c:when test="${not forceCommunityCreate}">
					<c:url var="url" value="/edit${url}">
						<c:param name="hash" value="${post.resource.intraHash}" />
						<c:param name="user" value="${post.user.name}" />
						<c:param name="copytag" value="${mtl:toTagString(post.tags)}" />
					</c:url>
				</c:when>
				<c:otherwise>
					<c:url var="url" value="/editGoldStandard${url}">
						<c:param name="hash" value="${post.resource.intraHash}" />
						<c:param name="user" value="${post.user.name}" />
						<c:param name="editBeforeSaving" value="true" />
					</c:url>
				</c:otherwise>
			</c:choose>
			
			<fmt:message key="${titleKey}" var="copyTitle"/>
		</c:otherwise>
	</c:choose>
	
	<a class="${buttonClasses}" href="${url}" title="${copyTitle}">
		<span class="fa fa-${icon}"><!--  --></span>
		<span class="sr-only"><fmt:message key="post.actions.copy"/></span>
	</a>
</jsp:root>