<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:spring="http://www.springframework.org/tags">

	<fmt:message key="navi.helpUrl" var="helpLangUrl" />

	<div id="footer-more">
		<nav class="row">
			<dl class="col-md-3 col-sm-6">
				<dt>
					<fmt:message key="footer.what">
						<fmt:param>${properties['project.name']}</fmt:param>
					</fmt:message>
					<c:if test="${command.context.loginUser.role eq 'ADMIN'}">
						&amp;nbsp;
						<c:set var="infoContent">
							<fmt:message key="build.version" >
								<fmt:param>${properties['project.name']}</fmt:param>
							</fmt:message>
							<br />
							(<fmt:message key="build.time" />)
						</c:set>
						<span class="fa fa-info-circle" data-container="body" data-toggle="popover" data-placement="top" data-content="${infoContent}"><!-- keep me --></span>
					</c:if>
				</dt>
				
				<dd><a href="/gettingStarted"><fmt:message key="footer.gettingStarted"/></a></dd>
				<dd><a href="/buttons"><fmt:message key="footer.buttons"/></a></dd>
				<dd><a href="${helpLangUrl}/Main"><fmt:message key="help"/></a></dd>
				
				<fmt:message key="footer.developer" var="developerFooter"/>
				<c:if test="${not empty developerFooter}">
					<dt><c:out value="${developerFooter}" /></dt>
					
					<dd><span class="fa fa-bitbucket"><!--  --></span><c:out value=" " /><a href="https://bibsonomy.bitbucket.io"><fmt:message key="footer.overview"/></a></dd>
					<dd><span class="fa fa-puzzle-piece"><!--  --></span><c:out value=" " /><a href="https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API"><fmt:message key="settings.apidoc"/></a></dd>
				</c:if>
			</dl>
			
			<dl class="col-md-3 col-sm-6">
				<dt><fmt:message key="footer.privacyContact"/></dt>
				
				<dd><fmt:message key="navi.about"/></dd>
				<dd>
					<fmt:message key="system.termsOfUse"/>
				</dd>
				<dd>
					<fmt:message key="footer.cookies"/>
				</dd>
				<dd>
					<fmt:message key="issues"/>
				</dd>
				<fmt:message key="footer.wiki" var="wikiFooter">
					<fmt:param value="${properties['project.name']}"/>
				</fmt:message>
				<c:if test="${not empty wikiFooter}">
					<dd>
						<c:out value="${wikiFooter}" escapeXml="false" />
					</dd>
				</c:if>
			</dl>
			
			<dl class="col-md-3 col-sm-6">
				<dt><fmt:message key="footer.integration"/></dt>
			
				<dd><a href="http://academic-puma.de/"><fmt:message key="footer.academicPuma"/></a></dd>
				<dd><a href="https://typo3.org/extensions/repository/view/ext_bibsonomy_csl"><fmt:message key="typoThree.plugin"/></a></dd>
				<dd><a href="https://wordpress.org/plugins/bibsonomy-csl/"><fmt:message key="wordpress.plugin"/></a></dd>
				<dd><a href="https://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-rest-client/"><fmt:message key="restClient.java"/></a></dd>
				<dd><a href="/scraperinfo"><fmt:message key="footer.scraperInfo" /></a></dd>
				<dd><a href="${helpLangUrl}/Integration"><fmt:message key="more"/></a></dd>
			</dl>
			
			<dl class="col-md-3 col-sm-6">
				<dt><fmt:message key="footer.about"/>&amp;nbsp;<c:out value="${properties['project.name']}" /></dt>
				
				<dd><fmt:message key="footer.team"/></dd>
				
				<c:set var="blogUrl" value="${properties['project.blog']}" />
				<c:if test="${not empty blogUrl}">
					<dd><a href="${blogUrl}"><fmt:message key="blog"/></a></dd>
				</c:if>
				
				<fmt:message key="footer.mailingList" var="footerMailingList" />
				<c:if test="${not empty footerMailingList}">
					<dd><a href="https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss"><c:out value="${footerMailingList}" /></a></dd>
				</c:if>
				
				<dt><fmt:message key="footer.socialMedia"/></dt>
				<c:set var="twitter" value="${properties['project.social.twitter']}" />
				<c:if test="${not empty twitter}">
					<dd><span class="fa fa-twitter"><!--  --></span>&amp;nbsp;<a href="https://twitter.com/${twitter}"><fmt:message key="footer.social.twitter"/></a></dd>
				</c:if>
				
				<c:set var="facebook" value="${properties['project.social.facebook']}" />
				<c:if test="${not empty facebook}">
					<dd><span class="fa fa-facebook"><!--  --></span>&amp;nbsp;<a href="https://facebook.com/${facebook}"><fmt:message key="footer.social.facebook"/></a></dd>
				</c:if>
			</dl>
		</nav>

		<div class="legal-notices">
			<hr />
			<p>
				<fmt:message key="system.offeredBy">
					<fmt:param>${properties['project.name']}</fmt:param>
				</fmt:message>
			</p>
		</div>
	</div>
</jsp:root>
