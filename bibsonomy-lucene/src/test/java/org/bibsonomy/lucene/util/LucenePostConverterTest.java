/**
 * BibSonomy - A blue social bookmark and publication sharing system.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.lucene.util;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.apache.lucene.document.Document;
import org.bibsonomy.common.enums.GroupID;
import org.bibsonomy.common.enums.Role;
import org.bibsonomy.lucene.index.LuceneFieldNames;
import org.bibsonomy.lucene.index.converter.LuceneResourceConverter;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.util.PersonNameParser.PersonListParserException;
import org.bibsonomy.search.LucenePost;
import org.bibsonomy.model.util.PersonNameUtils;
import org.bibsonomy.testutil.CommonModelUtils;
import org.bibsonomy.testutil.ModelUtils;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author fei
 */
public class LucenePostConverterTest {
	private static LuceneResourceConverter<BibTex> bibTexConverter;
	private static LuceneResourceConverter<Bookmark> bookmarkConverter;
	
	@SuppressWarnings("unchecked")
	@BeforeClass
	public static void setUp() {
		// use the configurated converters
		bibTexConverter = (LuceneResourceConverter<BibTex>) LuceneSpringContextWrapper.getBeanFactory().getBean("lucenePublicationConverter");
		bookmarkConverter = (LuceneResourceConverter<Bookmark>) LuceneSpringContextWrapper.getBeanFactory().getBean("luceneBookmarkConverter");
	}
	
	@Test
	public void writeBookmarkPost() {
		final LucenePost<Bookmark> refPost = generateBookmarkTestPost("testTitle", "testTag", "testUser", new Date(System.currentTimeMillis()), GroupID.PUBLIC);
		
		final Document doc = (Document) bookmarkConverter.readPost(refPost);
		
		final Post<Bookmark> testPost = bookmarkConverter.writePost(doc); 

		//--------------------------------------------------------------------
		// compare some elements
		//--------------------------------------------------------------------
		// title
		assertEquals(refPost.getResource().getTitle(), testPost.getResource().getTitle());

		// url
		assertEquals(refPost.getResource().getUrl(), testPost.getResource().getUrl());

		// tags
		for( final Tag tag : refPost.getTags() ) {
			assertEquals(true, testPost.getTags().contains(tag));
		}
		// hashes
		assertEquals(refPost.getResource().getIntraHash(), testPost.getResource().getIntraHash());
		assertEquals(refPost.getResource().getInterHash(), testPost.getResource().getInterHash());

		// groups
		for( final Group group : refPost.getGroups() ) {
			assertEquals(true, testPost.getGroups().contains(group));
		}
	}
	
	@Test
	public void writeBibTexPost() throws PersonListParserException {
		final LucenePost<BibTex> refPost = generateBibTexTestPost( "testTitle", "testTag", "testAuthor", "testUser", new Date(System.currentTimeMillis()), GroupID.PUBLIC);
		final Document doc = (Document) bibTexConverter.readPost(refPost);
		
		final Post<BibTex> testPost = bibTexConverter.writePost(doc); 

		//--------------------------------------------------------------------
		// compare some elements
		//--------------------------------------------------------------------
		// title
		assertEquals(refPost.getResource().getTitle(), testPost.getResource().getTitle());
		// url
		assertEquals(refPost.getResource().getUrl(), testPost.getResource().getUrl());
		// author
		assertEquals(refPost.getResource().getAuthor(), testPost.getResource().getAuthor());
		// year
		assertEquals(refPost.getResource().getYear(), testPost.getResource().getYear());
		// address
		assertEquals(refPost.getResource().getYear(), testPost.getResource().getYear());
		// tags
		for( final Tag tag : refPost.getTags() ) {
			assertEquals(true, testPost.getTags().contains(tag));
		}
		// hashes
		assertEquals(refPost.getResource().getIntraHash(), testPost.getResource().getIntraHash());
		assertEquals(refPost.getResource().getInterHash(), testPost.getResource().getInterHash());
		// groups
		for( final Group group : refPost.getGroups() ) {
			assertEquals(true, testPost.getGroups().contains(group));
		}
	}
	
	@Test
	public void bibTexPost() throws PersonListParserException {
		final LucenePost<BibTex> testPost = generateBibTexTestPost("testTitle", "testTag", "testAuthor", "testUser", new Date(System.currentTimeMillis()), GroupID.PUBLIC);
		
		final Document postDoc = (Document) bibTexConverter.readPost(testPost);
		
		//--------------------------------------------------------------------
		// compare some elements
		//--------------------------------------------------------------------
		// title
		assertEquals(testPost.getResource().getTitle(), postDoc.get(LuceneFieldNames.TITLE));
		// tags
		for( final Tag tag : testPost.getTags() ) {
			final String tagName = tag.getName();
			assertEquals(true, postDoc.get(LuceneFieldNames.TAS).contains(tagName));
		}
		// author
		assertEquals(PersonNameUtils.serializePersonNames(testPost.getResource().getAuthor()), postDoc.get(LuceneFieldNames.AUTHOR));
		// year
		assertEquals(testPost.getResource().getYear(), postDoc.get(LuceneFieldNames.YEAR));
		// address
		assertEquals(testPost.getResource().getAddress(), postDoc.get(LuceneFieldNames.ADDRESS));
		// groups
		for( final Group group : testPost.getGroups() ) {
			final String tagName = group.getName();
			assertEquals(true, postDoc.get(LuceneFieldNames.GROUP).contains(tagName));
		}
	}
	
	/**
	 * generate a BibTex Post, can't call setBeanPropertiesOn() because private
	 * so copy & paste the setBeanPropertiesOn() into this method
	 * 
	 * GroupID.PUBLIC
	 * @throws PersonListParserException 
	 */
	private static LucenePost<BibTex> generateBibTexTestPost(final String titleText, final String tagName, final String authorName, final String userName, final Date postDate, final GroupID groupID) throws PersonListParserException {
		final LucenePost<BibTex> post = createEmptyPost(BibTex.class, tagName, groupID, postDate, userName);
		
		final User user = new User();
		CommonModelUtils.setBeanPropertiesOn(user);
		user.setName(userName);
		user.setRole(Role.NOBODY);
		post.setUser(user);
		final BibTex resource = new BibTex();
		CommonModelUtils.setBeanPropertiesOn(resource);
		resource.setCount(0);		
		resource.setEntrytype("inproceedings");
		resource.setAuthor(PersonNameUtils.discoverPersonNames("MegaMan and Lucene GigaWoman "+authorName));
		resource.setEditor(PersonNameUtils.discoverPersonNames("Peter Silie "+authorName));
		resource.setTitle("bibtex insertpost test");
		
		resource.setTitle("title "+ (Math.round(Math.random()*Integer.MAX_VALUE))+" "+titleText); // TODO: random with seed
		resource.setYear("test year");
		resource.setJournal("test journal");
		resource.setBooktitle("test booktitle");
		resource.setVolume("test volume");
		resource.setNumber("test number");
		resource.setScraperId(-1);
		resource.setType("2");
		resource.recalculateHashes();
		post.setResource(resource);
		return post;
	}

	private static <T extends Resource> LucenePost<T> createEmptyPost(@SuppressWarnings("unused") final Class<T> rClass, final String tagName, final GroupID groupID, final Date postDate, final String userName) {
		final LucenePost<T> post = new LucenePost<T>();
		post.setContentId((int)Math.floor(Math.random()*Integer.MAX_VALUE)); // TODO: random with seed

		final Group group = new Group(groupID);
		post.getGroups().add(group);

		post.setTags(ModelUtils.getTagSet("tag1", "tag2", tagName));
		
		post.setContentId(null);
		post.setDescription("luceneTestPost");
		post.setDate(postDate);
		
		final User user = new User();
		CommonModelUtils.setBeanPropertiesOn(user);
		user.setName(userName);
		user.setRole(Role.NOBODY);
		post.setUser(user);
		
		return post;
	}

	/**
	 * generate a Bookmark Post, can't call setBeanPropertiesOn() because private
	 * so copy & paste the setBeanPropertiesOn() into this method
	 */
	private static LucenePost<Bookmark> generateBookmarkTestPost(final String titleText, final String tagName, final String userName, final Date postDate, final GroupID groupID) {
		final LucenePost<Bookmark> post = createEmptyPost(Bookmark.class, tagName, groupID, postDate, userName);
		
		final Bookmark resource = new Bookmark();
		resource.setCount(0);
		resource.setTitle("test"+(Math.round(Math.random()*Integer.MAX_VALUE))+" "+titleText); // TODO: random with seed
		resource.setUrl("http://www.test"+(Math.round(Math.random()*Integer.MAX_VALUE))+"url.org"); // TODO: random with seed
		resource.recalculateHashes();
		
		post.setResource(resource);
		return post;
	}
}
